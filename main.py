from flask import Flask, render_template, request, redirect, url_for
import psycopg2


app = Flask (__name__)

@app.route('/')
def index():
    return render_template('index.html', nombre_de_pagina='Pagina_principal', titulo_de_pagina='Bienvenidos a la pagina principal')

@app.route('/empleados')
def empleados():
    return render_template('empleados.html', nombre_de_pagina='empleados', titulo_de_pagina='Bienvenidos Empleados')

@app.route('/clases')
def clases():
    return render_template('clases.html',  nombre_de_pagina='Clases', titulo_de_pagina='Listasdo de Clases')

@app.route('/horarios')
def horarios():
    return render_template('horarios.html',  nombre_de_pagina='horarios', titulo_de_pagina='Horarios')

if __name__ == '__main__':
    app.run(debug=True)
